#ifndef VMETA_H
#define VMETA_H

#include <stdint.h>

typedef struct VM VM;

extern VM* initVM(uint64_t s);
extern int insertVM(VM* cc, uint64_t h1, uint64_t h2);
extern void switchModesVM(VM* cc , int startFrom);
extern int lookupKnownVM(VM* cc, uint64_t h1, uint64_t h2);
extern uint64_t sizeVM(VM* cc);
extern void freeVM(VM* cc);

#endif
