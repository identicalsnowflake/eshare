#include "vmeta.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// simple hash-table-ish structure for assigning auxiliary variables

struct VM {
  int cardinality;
  uint64_t* arr1;
  uint64_t* arr2;
  int* iarr;
  int len;
  int mask;
};

extern VM* initVM(uint64_t s) {
  printf("non-swiss");
  fflush( stdout );
  VM* vm = malloc(sizeof(struct VM));
  vm->cardinality = 0;
  int bc = pow(2,(int)s);
  vm->len = bc;
  vm->mask = bc - 1;
  vm->arr1 = calloc(2 * bc,sizeof(uint64_t));
  vm->arr2 = vm->arr1 + bc;
  vm->iarr = 0;
  return vm;
}

extern void searchF(VM* vm, uint64_t h1, uint64_t h2, int i) {
  uint64_t hi1 = vm->arr1[i];
  if(hi1 == 0) {
    vm->arr1[i] = h1;
    vm->arr2[i] = h2;
    vm->cardinality++;
  }else{
    uint64_t hi2 = vm->arr2[i];
    if(h1 != hi1 || h2 != hi2) {
      searchF(vm,h1,h2,i+1);
    }
  }
}

extern void searchB(VM* vm, uint64_t h1, uint64_t h2, int i) {
  uint64_t hi1 = vm->arr1[i];
  if(hi1 == 0) {
    vm->arr1[i] = h1;
    vm->arr2[i] = h2;
    vm->cardinality++;
  }else{
    uint64_t hi2 = vm->arr2[i];
    if(h1 != hi1 || h2 != hi2) {
      searchB(vm,h1,h2,i-1);
    }
  }
}

extern inline void insertNoResize(VM* vm, uint64_t h1, uint64_t h2) {
  int i = vm->mask & ((int)(h2));
  if(i < (vm->len - 1) / 2) {
    searchF(vm,h1,h2,i);
  }else{
    searchB(vm,h1,h2,i);
  }
}
 
extern inline void insertExistingF(VM* vm, uint64_t h1, uint64_t h2, int i) {
  uint64_t hi1 = vm->arr1[i];
  while(hi1 != 0) {
    ++i;
    hi1 = vm->arr1[i];
  }
  vm->arr1[i] = h1;
  vm->arr2[i] = h2;
}

extern inline void insertExistingB(VM* vm, uint64_t h1, uint64_t h2, int i) {
  uint64_t hi1 = vm->arr1[i];
  while(hi1 != 0) {
    --i;
    hi1 = vm->arr1[i];
  }
  vm->arr1[i] = h1;
  vm->arr2[i] = h2;
}

extern inline void insertExisting(VM* vm, uint64_t h1, uint64_t h2) {
  int i = vm->mask & ((int)(h2));
  if(i < (vm->len - 1) / 2) {
    insertExistingF(vm,h1,h2,i);
  }else{
    insertExistingB(vm,h1,h2,i);
  }  
}


extern int insertVM(VM* vm, uint64_t h1, uint64_t h2) {
  int oldc = vm->cardinality;
  int oldl = vm->len;
  if(2 * oldc > oldl) {
    vm->len = oldl * 2;
    vm->mask = (vm->mask << 1) + 1;
    uint64_t* olda1 = vm->arr1;
    uint64_t* olda2 = vm->arr2;
    uint64_t* newa1 = calloc(2 * vm->len,sizeof(uint64_t));
    uint64_t* newa2 = newa1 + vm->len;
    vm->arr1 = newa1;
    vm->arr2 = newa2;
    for(int i = 0; i < oldl ; ++i) {
      uint64_t h1 = olda1[i];
      if(h1 != 0) {
        insertExisting(vm,h1,olda2[i]);
      }
    }
    free(olda1);
    return insertVM(vm,h1,h2);
  }else{
    insertNoResize(vm,h1,h2);
    return oldc ^ vm->cardinality;
  }
}

// switch from accepting unknown inputs to tracking known hashes
extern void switchModesVM(VM* vm , int startFrom) {
  vm->iarr = calloc(vm->len , sizeof(int));
  vm->cardinality = startFrom;
}

extern int lookupExistingF(VM* vm, uint64_t h1, uint64_t h2, int i) {
  uint64_t hi1 = vm->arr1[i];
  while(hi1 != h1) {
    ++i;
    hi1 = vm->arr1[i];
  }
  
  if(vm->arr2[i] == h2) {
    int j = vm->iarr[i];
    if(j==0){
      j = vm->cardinality++;
      vm->iarr[i] = j;
      return j;
    }else{
      return -1 * j;
    }
  }else{
    return lookupExistingF(vm,h1,h2,i+1);
  }
}

extern int lookupExistingB(VM* vm, uint64_t h1, uint64_t h2, int i) {
  uint64_t hi1 = vm->arr1[i];
  while(hi1 != h1) {
    --i;
    hi1 = vm->arr1[i];
  }
  
  if(vm->arr2[i] == h2) {
    int j = vm->iarr[i];
    if(j==0){
      j = vm->cardinality++;
      vm->iarr[i] = j;
      return j;
    }else{
      return -1 * j;
    }
  }else{
    return lookupExistingB(vm,h1,h2,i-1);
  }
}

extern int lookupKnownVM(VM* vm, uint64_t h1, uint64_t h2) {
  int i = vm->mask & ((int)(h2));
  if(i < (vm->len - 1) / 2) {
    return lookupExistingF(vm,h1,h2,i);
  }else{
    return lookupExistingB(vm,h1,h2,i);
  }
}

extern uint64_t sizeVM(VM* vm) {
  return (uint64_t)(vm->cardinality);
}

extern void freeVM(VM* vm) {
  free(vm->arr1);
  free(vm->iarr);
  free(vm);
}

