#include "vmeta.h"
#include <math.h>
#include <stdint.h>
#include <stdlib.h>

// swiss-hash-table-ish structure for assigning auxiliary variables

struct VM {
  int cardinality;
  uint8_t* swiss;
  uint64_t* arr1;
  uint64_t* arr2;
  int* iarr;
  int len;
  int mask;
  int hasH;
};

extern VM* initVM(uint64_t s) {
  VM* vm = malloc(sizeof(struct VM));
  vm->cardinality = 0;
  int bc = pow(2,(int)s);
  vm->len = bc;
  vm->mask = bc - 1;
  vm->swiss = calloc(14 + bc,sizeof(uint8_t)) + 7;
  vm->arr1 = calloc(2*bc,sizeof(uint64_t));
  vm->arr2 = vm->arr1 + bc;
  vm->iarr = 0;
  vm->hasH = 0;
  return vm;
}

extern inline void assign(VM* vm, uint64_t h1, uint64_t h2, int i) {

  vm->swiss[i] = 0x80 | ((uint8_t)h2);
  vm->arr1[i] = h1;
  vm->arr2[i] = h2;
  vm->cardinality++;
}

#define CHECK_A(o) \
  if(!found_place && w == 0) { \
    assign(vm,h1,h2,i+(o)); \
    found_place = 1; \
  }else if(w == (0x80 | ((uint8_t)h2))) { \
    hi1 = vm->arr1[i+(o)]; \
    if(h1 == hi1) { \
      hi2 = vm->arr2[i+(o)]; \
      if(h2 == hi2) { \
        found_place = 1; \
      } \
    } \
  }

extern void searchF(VM* vm, uint64_t h1, uint64_t h2, int i) {

  uint64_t sw = *((uint64_t*)(vm->swiss + i));

  uint8_t found_place = 0;
  uint64_t hi1;
  uint64_t hi2;
  uint8_t w;

  w = ((uint8_t)(sw>>0));
  CHECK_A(0);
  
  w = ((uint8_t)(sw>>8));
  CHECK_A(1);
  
  w = ((uint8_t)(sw>>16));
  CHECK_A(2);
  
  w = ((uint8_t)(sw>>24));
  CHECK_A(3);
  
  w = ((uint8_t)(sw>>32));
  CHECK_A(4);
  
  w = ((uint8_t)(sw>>40));
  CHECK_A(5);
  
  w = ((uint8_t)(sw>>48));
  CHECK_A(6);
  
  w = ((uint8_t)(sw>>56));
  CHECK_A(7);
  
  
  if(!found_place) {
    searchF(vm,h1,h2,i+8);
  }
  
}

extern void searchB(VM* vm, uint64_t h1, uint64_t h2, int i) {
  
  uint64_t* swp = ((uint64_t*)(vm->swiss + i));
  swp = (uint64_t*)((uint8_t*)swp - 7);
  uint64_t sw = *swp;

  uint8_t found_place = 0;
  uint64_t hi1;
  uint64_t hi2;
  uint8_t w;

  w = ((uint8_t)(sw>>56));
  CHECK_A(0);
  
  w = ((uint8_t)(sw>>48));
  CHECK_A(-1);
  
  w = ((uint8_t)(sw>>40));
  CHECK_A(-2);
  
  w = ((uint8_t)(sw>>32));
  CHECK_A(-3);
  
  w = ((uint8_t)(sw>>24));
  CHECK_A(-4);
  
  w = ((uint8_t)(sw>>16));
  CHECK_A(-5);
  
  w = ((uint8_t)(sw>>8));
  CHECK_A(-6);
  
  w = ((uint8_t)(sw>>0));
  CHECK_A(-7);
  
  
  if(!found_place) {
    searchB(vm,h1,h2,i-8);
  }

}

/* extern void dump(uint64_t n) { */
/*     uint64_t i; */
/*     for (i = 1UL << 63; i > 0; i = i / 2) */
/*         (n & i) ? printf("1") : printf("0"); */
/* } */

extern inline void insertNoResize(VM* vm, uint64_t h1, uint64_t h2) {
  int i = vm->mask & ((int)(h1));
  if(i < (vm->len - 1) / 2) {
    searchF(vm,h1,h2,i);
  }else{
    searchB(vm,h1,h2,i);
  }
}
 
extern inline void insertExistingF(VM* vm, uint64_t h1, uint64_t h2, int i) {

  uint8_t found = 0;
  
  while(!found) {
    uint64_t k = *((uint64_t*)(vm->swiss + i));
    uint64_t sw = ~((k & 0x8080808080808080) | 0x7f7f7f7f7f7f7f7f) >> 7;
    if(sw!=0) {
      found = 1;
      int o = __builtin_ctzll(sw) / 8;
      vm->swiss[i+o] = 0x80 | (uint8_t)h2;
      vm->arr1[i+o] = h1;
      vm->arr2[i+o] = h2;
    }
    i+=8;
  }
  
}

extern inline void insertExistingB(VM* vm, uint64_t h1, uint64_t h2, int i) {
  
  uint8_t found = 0;
  
  while(!found) {
    uint64_t* swp = ((uint64_t*)(vm->swiss + i));
    swp = (uint64_t*)((uint8_t*)swp - 7);
    uint64_t sw = ~((*swp & 0x8080808080808080) | 0x7f7f7f7f7f7f7f7f);
    
    if(sw!=0) {
      found = 1;
      int o = -1 * (__builtin_clzll(sw) / 8);
      vm->swiss[i+o] = 0x80 | (uint8_t)h2;
      vm->arr1[i+o] = h1;
      vm->arr2[i+o] = h2;
    }
    i-=8;
  }
  
}

extern inline void insertExisting(VM* vm, uint64_t h1, uint64_t h2) {
  int i = vm->mask & ((int)(h1));
  if(i < (vm->len - 1) / 2) {
    insertExistingF(vm,h1,h2,i);
  }else{
    insertExistingB(vm,h1,h2,i);
  }  
}


extern int insertVM(VM* vm, uint64_t h1, uint64_t h2) {

  int oldc = vm->cardinality;
  int oldl = vm->len;
  
  if(oldl <= 512 ? 2 * oldc > oldl : 5 * oldc > 4 * oldl) {
    vm->len = oldl * 2;
    vm->mask = (vm->mask << 1) + 1;
    uint64_t* olda1 = vm->arr1;
    uint64_t* olda2 = vm->arr2;
    uint8_t* oldsw = vm->swiss;
    uint8_t* newsw = calloc(14 + vm->len,sizeof(uint8_t)) + 7;
    uint64_t* newa1 = calloc(2 * vm->len,sizeof(uint64_t));
    uint64_t* newa2 = newa1 + vm->len;
    vm->swiss = newsw;
    vm->arr1 = newa1;
    vm->arr2 = newa2;
    
    for(int i = 0; i < oldl ; ++i) {
      uint64_t h1 = olda1[i];      
      if(h1 != 0) {
        insertExisting(vm,h1,olda2[i]);
      }
    }
    free(oldsw - 7);
    free(olda1);
    return insertVM(vm,h1,h2);
  }else{
    insertNoResize(vm,h1,h2);
    return oldc ^ vm->cardinality;
  }
}

// switch from accepting unknown inputs to tracking known hashes
extern void switchModesVM(VM* vm , int startFrom) {
  vm->iarr = calloc(vm->len , sizeof(int));
  vm->cardinality = startFrom;
}

#define LOOKUP(o) \
    hi2 = vm->arr2[i+(o)]; \
    if(h2==hi2) { \
      hi1 = vm->arr1[i+(o)]; \
      if(h1 == hi1) { \
	int j = vm->iarr[i+(o)]; \
	if(j==0){ \
	  j = vm->cardinality++; \
	  vm->iarr[i+(o)] = j;	 \
	  return j; \
	}else{ \
	  return -1*j; \
	} \
      } \
    }

extern int lookupExistingF(VM* vm, uint64_t h1, uint64_t h2, int i) {

  uint64_t sw = *((uint64_t*)(vm->swiss + i));
  uint8_t w = 0x80 | ((uint8_t)h2);

  uint64_t hi1;
  uint64_t hi2;

  if(w == (uint8_t)(sw >> 0)) {
    LOOKUP(0);
  }
  
  if(w == (uint8_t)(sw >> 8)) {
    LOOKUP(1);
  }
  
  if(w == (uint8_t)(sw >> 16)) {
    LOOKUP(2);
  }
  
  if(w == (uint8_t)(sw >> 24)) {
    LOOKUP(3);
  }
  
  if(w == (uint8_t)(sw >> 32)) {
    LOOKUP(4);
  }
  
  if(w == (uint8_t)(sw >> 40)) {
    LOOKUP(5);
  }
  
  if(w == (uint8_t)(sw >> 48)) {
    LOOKUP(6);
  }
  
  if(w == (uint8_t)(sw >> 56)) {
    LOOKUP(7);
  }

  lookupExistingF(vm,h1,h2,i+8);
  
}

extern int lookupExistingB(VM* vm, uint64_t h1, uint64_t h2, int i) {

  uint64_t* swp = ((uint64_t*)(vm->swiss + i));
  swp = (uint64_t*)((uint8_t*)swp - 7);
  uint64_t sw = *swp;

  uint8_t w = 0x80 | ((uint8_t)h2);

  uint64_t hi1;
  uint64_t hi2;

  if(w == (uint8_t)(sw >> 56)) {
    LOOKUP(0);
  }
  
  if(w == (uint8_t)(sw >> 48)) {
    LOOKUP(-1);
  }
  
  if(w == (uint8_t)(sw >> 40)) {
    LOOKUP(-2);
  }
  
  if(w == (uint8_t)(sw >> 32)) {
    LOOKUP(-3);
  }
  
  if(w == (uint8_t)(sw >> 24)) {
    LOOKUP(-4);
  }
  
  if(w == (uint8_t)(sw >> 16)) {
    LOOKUP(-5);
  }
  
  if(w == (uint8_t)(sw >> 8)) {
    LOOKUP(-6);
  }
  
  if(w == (uint8_t)(sw >> 0)) {
    LOOKUP(-7);
  }

  lookupExistingB(vm,h1,h2,i-8);

}

extern int lookupKnownVM(VM* vm, uint64_t h1, uint64_t h2) {
  int i = vm->mask & ((int)(h1));
  if(i < (vm->len - 1) / 2) {
    return lookupExistingF(vm,h1,h2,i);
  }else{
    return lookupExistingB(vm,h1,h2,i);
  }
}

extern uint64_t sizeVM(VM* vm) {
  return (uint64_t)(vm->cardinality);
}

extern void freeVM(VM* vm) {
  free(vm->swiss - 7);
  free(vm->arr1);
  free(vm->iarr);
  free(vm);
}

