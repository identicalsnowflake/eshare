{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -O2 #-}

-- | An alternative to Data.Hashable better suited for the purposes of this package, both due to larger hash size resulting in much lower probability of collision and due to certain critical hash choices in Data.Hashable being perfectly-suboptimal for use with hash tables: namely, the @Hashable@ instance for @Int@ is the identity function, resulting in a pathological bit distribution for consecutive integers when used with a masking hash table. In contrast, the implementation in this package uses a Knuth-style prime multiplication to obtain a random-looking bit distribution across all 128 bits for consecutive integers.
--
-- Be aware that the Generic-derived @Hashable128@ instance uses purely structural information, meaning that isomorphic types differing only in metadata information will have the same derived hash. The same is obviously true for newtype-derived instances.

module EShare.Hashable128
  ( Hash128(..)
  , (<+>)
  , Hashable128(..)
  ) where

import qualified Data.Bits as B
import Data.Foldable
import Data.Void
import Data.Fix (Fix(..))
import Foreign.C.Types
import Foreign.Ptr
import GHC.Exts
import GHC.Float
import GHC.Generics
import GHC.Int
import GHC.Word
import System.Posix.Types


data Hash128 = Hash128 {-# UNPACK #-} !Int64 {-# UNPACK #-} !Int64 deriving (Eq,Ord,Read,Show)

infixr 6 <+>
(<+>) :: Hash128 -> Hash128 -> Hash128
(<+>) (Hash128 x1 x2) (Hash128 y1 y2) = Hash128 ((hashInt $ B.rotate y2 39 `B.xor` 1003085972612599214) `B.xor` (B.rotate y1 17 + B.rotate x2 (-9)))
                                                (x2 + do B.rotate x1 13 `B.xor` B.rotate y2 (-2))


class Hashable128 a where
  {-# INLINE hash128 #-}
  hash128 :: a -> Hash128
  default hash128 :: (Generic a , GHashable128 a (Rep a)) => a -> Hash128
  hash128 = ghash128 @a @(Rep a) . from

instance Hashable128 Hash128 where
  {-# INLINE hash128 #-}
  hash128 = id

instance (Hashable128 a , Hashable128 b) => Hashable128 (a , b) where
  {-# INLINE hash128 #-}
  hash128 (x , y) = Hash128 5548721481829065912 (-5978969750488161314) <+> hash128 x <+> hash128 y

instance (Hashable128 a , Hashable128 b , Hashable128 c) => Hashable128 (a , b , c) where
  {-# INLINE hash128 #-}
  hash128 (x , y , z) = Hash128 5762940778754831113 2031162193133012795
    <+> hash128 x <+> hash128 y <+> hash128 z

instance Hashable128 a => Hashable128 [ a ] where
  hash128 = foldl' (\a x -> a <+> hash128 x) (Hash128 1044062377806547561 2510240225592276566)

instance Hashable128 () where
  {-# INLINE hash128 #-}
  hash128 _ = Hash128 (-4753542638980577819) (-2132637206635788611)

instance Hashable128 a => Hashable128 (Maybe a) where
  {-# INLINE hash128 #-}
  hash128 Nothing = Hash128 (-1754434270720046120) 8413676397340993691
  hash128 (Just x) = Hash128 (-5113402443720695613) (-683855113873052931) <+> hash128 x

instance Hashable128 Bool where
  {-# INLINE hash128 #-}
  hash128 True = Hash128 851030815052850088 5061451078196606040
  hash128 False = Hash128 (-1473303290026691597) (-6698029831172833309)

instance (Hashable128 a , Hashable128 b) => Hashable128 (Either a b) where
  {-# INLINE hash128 #-}
  hash128 (Left x) = Hash128 (-8891834758340146300) 3913408871607442007 <+> hash128 x
  hash128 (Right x) = Hash128 (-1858549056880891921) 4424248943313179883 <+> hash128 x

instance Hashable128 Void where
  hash128 _ = undefined

instance Hashable128 Int64 where
  {-# INLINE hash128 #-}
  hash128 k =
    Hash128 (hashInt (k - 601669210853403257)) (hashInt (k + 3110452463631465452))

instance Hashable128 Word where
  {-# INLINE hash128 #-}
  hash128 (W# k) = hash128 (I64# (intToInt64# (word2Int# k)))

instance Hashable128 Double where
  {-# INLINE hash128 #-}
  hash128 k =
    Hash128 (hashInt (doubleToInt k - 5971217309757540656)) (hashInt (doubleToInt k - 3748846926547954546))

{-# INLINE doubleToInt #-}
doubleToInt :: Double -> Int64
doubleToInt d = case castDoubleToWord64 d of
  W64# i -> I64# (intToInt64# (word2Int# (word64ToWord# i)))

instance Hashable128 Float where
  {-# INLINE hash128 #-}
  hash128 = hash128 . float2Double

instance Hashable128 Int8 where
  {-# INLINE hash128 #-}
  hash128 (I8# i) = hash128 (I64# (intToInt64# (int8ToInt# i)))

instance Hashable128 Int16 where
  {-# INLINE hash128 #-}
  hash128 (I16# i) = hash128 (I64# (intToInt64# (int16ToInt# i)))

instance Hashable128 Int32 where
  {-# INLINE hash128 #-}
  hash128 (I32# i) = hash128 (I64# (intToInt64# (int32ToInt# i)))

instance Hashable128 Int where
  {-# INLINE hash128 #-}
  hash128 (I# i) = hash128 (I64# (intToInt64# i))

instance Hashable128 Word8 where
  {-# INLINE hash128 #-}
  hash128 (W8# i) = hash128 (W# (word8ToWord# i))

instance Hashable128 Word16 where
  {-# INLINE hash128 #-}
  hash128 (W16# i) = hash128 (W# (word16ToWord# i))

instance Hashable128 Word32 where
  {-# INLINE hash128 #-}
  hash128 (W32# i) = hash128 (W# (word32ToWord# i))

instance Hashable128 Word64 where
  {-# INLINE hash128 #-}
  hash128 (W64# i) = hash128 (W# (word64ToWord# i))

deriving newtype instance (Hashable128 (f (Fix f))) => Hashable128 (Fix f)
deriving newtype instance Hashable128 (f (g a)) => Hashable128 ((f :.: g) a)
instance (Hashable128 (f a) , Hashable128 (g a)) => Hashable128 ((f :*: g) a) where
  {-# INLINE hash128 #-}
  hash128 (a :*: b) = hash128 (a , b)
instance (Hashable128 (f a) , Hashable128 (g a)) => Hashable128 ((f :+: g) a) where
  {-# INLINE hash128 #-}
  hash128 (L1 x) = hash128 @(Either (f a) (g a)) (Left x)
  hash128 (R1 x) = hash128 @(Either (f a) (g a)) (Right x)

deriving newtype instance Hashable128 IntPtr
deriving newtype instance Hashable128 WordPtr
deriving newtype instance Hashable128 CUIntMax
deriving newtype instance Hashable128 CIntMax
deriving newtype instance Hashable128 CUIntPtr
deriving newtype instance Hashable128 CIntPtr
deriving newtype instance Hashable128 CSigAtomic
deriving newtype instance Hashable128 CWchar
deriving newtype instance Hashable128 CSize
deriving newtype instance Hashable128 CPtrdiff
deriving newtype instance Hashable128 CBool
deriving newtype instance Hashable128 CULLong
deriving newtype instance Hashable128 CLLong
deriving newtype instance Hashable128 CULong
deriving newtype instance Hashable128 CLong
deriving newtype instance Hashable128 CUInt
deriving newtype instance Hashable128 CInt
deriving newtype instance Hashable128 CUShort
deriving newtype instance Hashable128 CShort
deriving newtype instance Hashable128 CUChar
deriving newtype instance Hashable128 CSChar
deriving newtype instance Hashable128 CChar
deriving newtype instance Hashable128 Fd
deriving newtype instance Hashable128 CNfds
deriving newtype instance Hashable128 CSocklen
deriving newtype instance Hashable128 CKey
deriving newtype instance Hashable128 CId
deriving newtype instance Hashable128 CFsFilCnt
deriving newtype instance Hashable128 CFsBlkCnt
deriving newtype instance Hashable128 CClockId
deriving newtype instance Hashable128 CBlkCnt
deriving newtype instance Hashable128 CBlkSize
deriving newtype instance Hashable128 CRLim
deriving newtype instance Hashable128 CTcflag
deriving newtype instance Hashable128 CUid
deriving newtype instance Hashable128 CNlink
deriving newtype instance Hashable128 CGid
deriving newtype instance Hashable128 CSsize
deriving newtype instance Hashable128 CPid
deriving newtype instance Hashable128 COff
deriving newtype instance Hashable128 CMode
deriving newtype instance Hashable128 CIno
deriving newtype instance Hashable128 CDev

-- splitmix-ish
{-# INLINE hashInt #-}
hashInt :: Int64 -> Int64
hashInt x = do
  let a = (x `B.xor` (B.shiftR x 30)) * (-4658895280553007687)
      b = (a `B.xor` (B.shiftR a 27)) * (-7723592293110705685)
  b `B.xor` (B.shiftR b 31)


class GHashable128 s f where
  ghash128 :: f a -> Hash128

instance GHashable128 s V1 where
  ghash128 = undefined

instance GHashable128 s U1 where
  {-# INLINE ghash128 #-}
  ghash128 _ = hash128 ()

instance Hashable128 b => GHashable128 w (K1 i b) where
  {-# INLINE ghash128 #-}
  ghash128 (K1 x) = hash128 x

instance (GHashable128 s a , GHashable128 s b) => GHashable128 s (a :*: b) where
  {-# INLINABLE ghash128 #-}
  ghash128 (a :*: b) = Hash128 (-8939872030008444335) 696645630922582428 <+> ghash128 @s a <+> ghash128 @s b

instance (GHashable128 s a , GHashable128 s b) => GHashable128 s (a :+: b) where
  {-# INLINABLE ghash128 #-}
  ghash128 (L1 x) = Hash128 (-4727611574311338312) 4519907304766809689 <+> ghash128 @s x
  ghash128 (R1 x) = Hash128 3227613844197169983 8345027274611344536 <+> ghash128 @s x

instance GHashable128 s f => GHashable128 s (M1 i t f) where
  {-# INLINE ghash128 #-}
  ghash128 (M1 x) = ghash128 @s @f x

