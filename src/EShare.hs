-- | EShare is a low-level utility for recovering sharing in EDSL expressions. Operation takes place in two phases: deduplication and assignment. It is backed by a mutable hash table in C, and as such should be reasonably performant, capable of several million insertions per second.

module EShare
  ( new
  , insert
  , cardinality
  , switchModes
  , assignKnown
  , free
  , type DeDuper
  , type Assigner
  , module EShare.Hashable128
  ) where

import Control.Monad.ST
import Control.Monad.ST.Unsafe
import Data.Functor
import GHC.Exts
import GHC.Int
import GHC.Word
import Foreign.C.Types

import EShare.Hashable128


newtype DeDuper s = DeDuper (Ptr ())
newtype Assigner s = Assigner (Ptr ())

foreign import ccall unsafe "static vmeta.h initVM" initVM :: Word64 -> IO (Ptr ())
foreign import ccall unsafe "static vmeta.h insertVM" insertVM :: Ptr () -> Int64 -> Int64 -> IO CInt
foreign import ccall unsafe "static vmeta.h sizeVM" sizeVM :: Ptr () -> IO Word64
foreign import ccall unsafe "static vmeta.h switchModesVM" switchModesVM :: Ptr () -> CInt -> IO ()
foreign import ccall unsafe "static vmeta.h lookupKnownVM" lookupKnownVM :: Ptr () -> Int64 -> Int64 -> IO CInt
foreign import ccall unsafe "static vmeta.h freeVM" freeVM :: Ptr () -> IO ()

{-# INLINE new #-}
-- | Initialize the tracker with the given bitmask size @s@. The number of buckets in the underlying hash table will be @2^s@. Memory will not be freed until @free@ is called.
new :: Word64 -> ST s (DeDuper s)
new s = DeDuper <$> unsafeIOToST (initVM s)

{-# INLINE insert #-}
-- | Insert the hash into the tracker. Returns 0 if the pair was already in the tracker; returns 1 if the pair was freshly inserted.
insert :: DeDuper s -> Hash128 -> ST s CInt
insert (DeDuper p) (Hash128 w1 w2) = unsafeIOToST (insertVM p w1 w2)

{-# INLINE cardinality #-}
-- | The number of unique hashes inserted.
cardinality :: DeDuper s -> ST s Int
cardinality (DeDuper p) = unsafeIOToST (sizeVM p) <&> \case
  W64# i -> I# (word2Int# (word64ToWord# i))

{-# INLINE switchModes #-}
-- | Switch from the insertion phase to the lookup phase. Further use of the given deduplicator is undefined. The integer input is the starting value for which identifiers will be assigned, in consecutive order.
switchModes :: DeDuper s -> CInt -> ST s (Assigner s)
switchModes (DeDuper p) i = do
  unsafeIOToST (switchModesVM p i)
  pure (Assigner p)

{-# INLINE assignKnown #-}
-- | Assign an identifier to a known hash. The hash must have been inserted during the insertion phase, otherwise behavior us undefined. The return value is a variable assignment. If the return value is negative, this hash has been looked up before by a prior call to @lookupKnown@ (and its assigned identifier may be recovered by negation); if the return value is positive, this is the first call to @lookupKnown@ with this hash pair, and the returned identifier is fresh.
assignKnown :: Assigner s -> Hash128 -> ST s CInt
assignKnown (Assigner p) (Hash128 w1 w2) = unsafeIOToST (lookupKnownVM p w1 w2)

{-# INLINE free #-}
-- | Deallocate the memory for the tracker. Further use is undefined.
free :: Assigner s -> ST s ()
free (Assigner p) = unsafeIOToST (freeVM p)

